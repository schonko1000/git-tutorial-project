FROM python:3.9

RUN python3 -m pip install flask
RUN python3 -m pip install psycopg2-binary

ADD . .

EXPOSE 5000

CMD python3 main.py

