import psycopg2
from flask import Flask, request
app = Flask(__name__)

HOST_URL = 'ip_address'


@app.route('/create_table', methods=['GET', 'POST'])
def create_table():
    with psycopg2.connect(
            host=HOST_URL,
            database="postgres",
            user="postgres",
            password="postgres",
            port=5440) as conn:
        cursor = conn.cursor()

        try:
            cursor.execute('drop table records;')
        except:
            pass
        try:
            cursor.execute(f"create table records (id integer, text varchar)")
            return "Table created"
        except:
            return "Problem"
        finally:
            cursor.close()

@app.route('/add_record', methods=['GET', 'POST'])
def add_record():
    with psycopg2.connect(
            host=HOST_URL,
            database="postgres",
            user="postgres",
            password="postgres",
            port=5440) as conn:

        cursor = conn.cursor()

        record = request.values.get('data')

        cursor.execute(f"insert into records (text) VALUES('{record}')")

        cursor.execute(f"select * from records;")
        results = cursor.fetchall()

        cursor.close()

        return {"response": results}

if __name__ == '__main__':
    app.run(host='0.0.0.0')

